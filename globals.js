var Path = require('path');
function Globals() {
};


//Mysql DB configurations
Globals.prototype.MySqlPort = 3306;
Globals.prototype.MySqlDB = "TempDB";
Globals.prototype.MySqlUser = "root";
Globals.prototype.MySqlHost = 'localhost';
Globals.prototype.MySqlPass = "blaze";
Globals.prototype.appRoot = Path.resolve(__dirname);
Globals.prototype.LoggerConfig = {
    appenders: [
        {type: 'console'},
        {
            "type": "file",
            "filename": Globals.prototype.appRoot + "/logs/log_file.log",
            "maxLogSize": 10485760,
            "backups": 10,
            "category": "relative-logger"
        }
    ]
};


module.exports = new Globals();
