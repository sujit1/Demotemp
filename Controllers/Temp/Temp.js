var express = require('express')
    , router = express.Router()
    , _ = require('underscore')
    , mysqlDb = require('../../mySqlDB')
    , Globals = require('../../globals')
    , log4js = require('log4js');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

function Temp(){
    _.bindAll(this,"getmodel","insertTemparature","getTemparature");
    
    router.post("/insertTemparature",this.insertTemparature);
    router.get("/getTemparature",this.getTemparature);
    router.post("/sample",this.sample);
    return router;
};


Temp.prototype.sample=function(req,res)
{
  console.log("sample call success");
  res.send({status:1 ,message:"i am  connected "})
}

Temp.prototype.getTemparature=function(req,res)
{
  var that=this;
  that.getmodel();
  that.TempData.findAll()
  .then(function(TempatureData){
   
      if(TempatureData[0])
      {
        res.send({status:1,message:"Data retrived successfully",data:TempatureData})
      }
      else
      {
          res.send({status:0,message:"No data Avalible"});
      }
  })
  .catch(function(err){
    res.send({status:0,message:"Error in retriving data" + err});
  });
};
Temp.prototype.insertTemparature=function(req,res)
{
     var that=this;
     that.getmodel();
     var dataObj=req.body.reqObj;
     that.TempData.build(dataObj).save()
     .then(function(tempData){
          if(tempData)
          {
            res.send({status:1,message:"data inserted successfully", data:tempData});
          }
          else
          {
            res.send({status:0,message:"No data inserted" });
          }
     })
     .catch(function(err){
        res.send({status:0,message:"Error in saving data" + err});
     })

};

Temp.prototype.getmodel=function()
{
    var Models = mysqlDb.getModels();
    this.TempData=Models.Temp;
};

module.exports = new Temp();
