var Sequelize = require('sequelize');
var Temp = require('./Models/Temp');
var models = {};

exports.connect = function(host, port, dbName, userName, password,cb){
    var sequelize = new Sequelize(dbName, userName, password, {
        host: host,
        port: port,
        dialect: 'mysql',
        logging: false,
        syncOnAssociation: true,
        pool: {
            max: 5000,
            min: 2,
            idle: 10000
          },
        define: {  charset: 'utf8',
            collate: 'utf8_unicode_ci',
            timestamps: false }
    });

    models.Temp = sequelize.define('Temp', Temp.getSchema(), {tableName: Temp.getTableName()});
    // sequelize.sync({force:true}).then(function(){    // Note: This will delete the tables and create them again.
    sequelize.sync({alter: true}).then(function(){
        mySqlConnection = sequelize;
        cb();
    }).catch(function(err){
        console.log("Error while connecting to Db: " + err.message);
    });
};

exports.getModels = function(){
    return models;
};

