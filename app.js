
var express    = require('express'),
app        = express(),
fs = require('fs'),
https = require('https'),
db = require('./mySqlDB'),
bodyParser = require('body-parser'),
port = process.env.PORT || 3000,
Globals = require('./globals'),
log4js = require('log4js'),
_ = require('underscore');


log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');


app.set('views', __dirname + '/Views');
app.engine('html', require('ejs').__express);
app.set('view engine', 'ejs');


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(logResponseBody);
app.use(require('./Controllers'));


function logResponseBody(req, res, next) {
  var oldWrite = res.write,
  oldEnd = res.end,
  chunks = [],
  t1 = new Date();

  res.write = function (chunk) {
    chunks.push(chunk);
    oldWrite.apply(res, arguments);
  };

  res.end = function (chunk) {
    if (chunk)
    chunks.push(chunk);

    //        var body = Buffer.concat(chunks).toString('utf8');
    var t2 = new Date();
    //        logger.trace((t2 - t1) + " : Path: " + req.path + " :Req.body:::: " + JSON.stringify(req.body) + " : ResponseBody:::: "+ body);

    oldEnd.apply(res, arguments);
  };

  next();
};


process.on('SIGINT', function() {
  process.exit(0);
});

process.on('uncaughtException', function(err) {
  // handle the error safely
  logger.info(err.stack);
});

db.connect(Globals.MySqlHost, Globals.MySqlPort, Globals.MySqlDB, Globals.MySqlUser, Globals.MySqlPass, function (err) {

  console.log("mysql  connection  "+Globals.MySqlHost + ': ' + Globals.MySqlPort);
  if (err) {
    logger.error('Unable to connect to MySql.');
    process.exit(1);
  } else {
    logger.info('connected to MySql.');
    app.listen(port, function () {
      logger.info('API\'s work at http://localhost:' + port + " url.");
    });
  }
});
