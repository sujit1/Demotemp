var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    temp_value: { type: Sequelize.STRING },
    temp_recording_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
   
  };
};

exports.getTableName = function(){
  return "temp_data";
};
